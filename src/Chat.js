import React, { Component } from 'react'
import logo from './logo.jpg';
import './Chat.css';
import Header from './components/header/Header.js';
import Message from './components/message/Message.js';
import OwnMessage from './components/own-message/OwnMessage.js';
import MessageInput from './components/message-input/MessageInput.js';
import MessageList from './components/message-list/MessageList.js';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import Preloader from './components/preloader/Preloader';

import './components/preloader/Preloader.css';

class Chat extends Component {
  constructor(props) {
    super(props)
    this.state = {
      messages: {
        isLoading: false,
        data: []
      },
      editModal: {
        isOpen: false,
        editedMessage: null
      },
      messageInput: {
        message: ''
      },
      url: ""
    };
    this.handleMessageDelete = this.handleMessageDelete.bind(this);
  }

  componentDidMount() {

    fetch(this.props.url)
      .then((response) => response.json())
      .then((message) => {
        this.setState({
          messages: {
            isLoading: true,
            data: this.sortByDate(message),
          }
        });
      })
  }

  handleNewMessage = (text) => {
    this.setState({
      messages: {
        isLoading: true,
        data: [...this.state.messages.data, { id: uuidv4(), me: true, user: "Me", text: text, createdAt: new Date().toISOString() }],
      }
    })
  }

  handlerCountParticipants = () => {
    const list = new Set();
    this.state.messages.data.map((message, i) => {
      list.add(message.user);
    });
    return list.size;
  }

  handlerCountMessages() {
    return this.state.messages.data.length;
  }

  handlerDateTimeLastMessage = () => {
    var lastMessage = this.state.messages.data[this.state.messages.data.length - 1];
    var lastdate = lastMessage?.createdAt;
    return lastdate;
  }

  sortByDate(messages) {
    var sortMessages = messages.sort(function (a, b) {
      var c = new Date(a.createdAt).getTime();
      var d = new Date(b.createdAt).getTime();
      if (c > d) {
        return 1;
      }
      if (c < d) {
        return -1;
      }
      return 0;
    });

    return sortMessages;
  }

  handleMessageDelete(id) {

    const index = this.state.messages.data.findIndex(function (o) {
      return o.id === id;
    })
  }

  render() {
    return (
      <div className="chat">
        <Preloader isLoading={this.state.messages.isLoading} />
        <Header
          countParticipants={this.handlerCountParticipants()}
          countMessages={this.handlerCountMessages()}
          dateTimeLastMessage={this.handlerDateTimeLastMessage()}
          messages={this.state.messages.data}
        />
        <MessageList
          messages={this.state.messages.data}
          onMessageDelete={this.handleMessageDelete}
        />
        <MessageInput onMessageSend={this.handleNewMessage} />

        <header className="logo-wrapper">
          <img src={logo} className="main-logo" alt="logo" />
        </header>
      </div>
    );
  }
}

export default Chat;
