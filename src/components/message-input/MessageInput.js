import React, { Component } from 'react';
import PropTypes from 'prop-types'

import './MessageInput.css';

class MessageInput extends React.Component {
  static propTypes = {
    onMessageSend: PropTypes.func.isRequired,
  }

  componentDidMount = () => {
    this.input.focus()
  }

  handleFormSubmit = (event) => {
    event.preventDefault()
    this.props.onMessageSend(this.input.value)
    this.input.value = ""
  }

  render() {
    return (
      <form className="message-input" onSubmit={this.handleFormSubmit}>
        <div className="message-input-text">
          <input
            type="text"
            ref={(node) => (this.input = node)}
            placeholder="Enter your message..."
          />
        </div>
        <div className="message-input-button">
          <button type="submit">
            Send
          </button>
        </div>
      </form>
    );
  }
}

export default MessageInput;

