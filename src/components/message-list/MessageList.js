import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Message from '../message/Message.js';

import './MessageList.css';
import OwnMessage from '../own-message/OwnMessage.js';

class MessageList extends Component {
    constructor(props) {
        super(props);
        this.onMessageDelete = this.onMessageDelete.bind(this);

    }
    static propTypes = {
        messages: PropTypes.arrayOf(PropTypes.object)
    }

    userMessage(message, i) {
        const isUser = message.user;
        if (isUser === "Me") {
            return <OwnMessage 
            key={i} {...message} 
            onMessageDelete={this.onMessageDelete} 
            />;
        }
        return <Message key={i} {...message} />;
    }

    onMessageDelete (id) {
        this.props.onMessageDelete(id);
        
    }

    compareDate(date, index) {
        if (index == 0) {
            this.setDivider(date);
        }
        var minDate = new Date(this.minDate).setHours(0, 0, 0, 0);
        var date2 = new Date(date).setHours(0, 0, 0, 0);

        if (minDate < date2) {

            this.minDate = date;
            this.setDivider();
        }
    }

    setDivider() {
        let div = document.createElement('div');
        div.id = 'message-divider';
        document.getElementsByClassName("messageList")[0].appendChild(div);
    }

    render() {
        this.minDate = this.props.messages[0]?.createdAt;
        return (
            <div className="messageList">
                {this.props.messages.map((message, i) => (
                    this.compareDate(message.createdAt, i)
                    ,
                    this.userMessage(message, i)
                ))}

            </div>
        );
    }
}

export default MessageList;
