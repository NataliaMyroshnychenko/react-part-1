import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Message.css';
import moment from 'moment';

class Message extends Component {

  static propTypes = {
    user: PropTypes.string,
    text: PropTypes.string.isRequired,
    createdAt: PropTypes.string,
    me: PropTypes.bool,
  }

  colorLike() {
    document.getElementById('like').onclick = function () {
      this.style.backgroundColor = "red";
    }
  }

  render() {
    return (

      <div className="message">
        <div className="message-user-avatar">

          <img src={this.props.avatar} alt="Аватар" />
        </div>
        <div className="message-user-name">
          {this.props.user}
        </div>
        <div className="message-text">
          {this.props.text}
        </div>
        <div className="message-time">
          {moment(this.props.createdAt).format('HH:mm')}
        </div>
        <div className="message-like">
          <button id="like" onClick={this.colorLike}>
            Like
          </button>
        </div>

      </div>

    )
  }
}

export default Message;